# lens-enterprise-api

API for interacting with the Enterprise Service.

## Implementing webhooks.
If you are interested in being notified when a user shares, 
edits, or revokes a Lens then you need webhooks! We have deployed
a complete example application that walks you through using webhooks
[RIGHT HERE](https://gitlab.com/MyLens/example-webhooks#example-webhooks).