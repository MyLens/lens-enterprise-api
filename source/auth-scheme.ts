export interface IAuth {
    generateAuthenticationHeader() : {[auth: string]: string} | null; 
    isAuthenticated(): boolean; 
}

export class BasicAuth implements IAuth {
    private userId: string; 
    private password: string;
    
    constructor(userId: string, password: string){
        this.userId = userId; 
        this.password = password; 
    }

    isAuthenticated() {
        return !!(this.userId && this.password); 
    }

    generateAuthenticationHeader() : {[auth: string]: string} | null {
        if (this.userId && this.password) {
            return {Authorization: "Basic " + Buffer.from(`${this.userId}:${this.password}`).toString('base64')}; 
        } 
        return null; 
    }
}

export class LensAuth implements IAuth {
    private _accessToken: string;

    constructor(accessToken: string){
        this._accessToken = accessToken; 
    }

    isAuthenticated() {
        return !!this._accessToken;
    }
    
    generateAuthenticationHeader(): {[auth: string]: string} | null {
        if (this.isAuthenticated()) {
            return { Authorization: `Bearer ${this._accessToken}` };
        }
        return null; 
    }
}