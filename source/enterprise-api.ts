import Axios, { AxiosInstance } from 'axios';
import { 
    Project, 
    UProject,
    PublicProject,
    LensRef, 
    ULensRef,
    LensRefBatchRequest,
    LensRefBatchResponse,
    User,
    UUser,
    CUser,
    Invitation,
    CInvitation,
    EnterpriseServiceError 
} from './types'

import {IAuth} from './auth-scheme'; 

export default class EnterpriseApi {
    private axios: AxiosInstance;
    private auth?: IAuth; 
    constructor(apiHost :any , auth? : IAuth) {
        this.auth = auth;

        const headers = Object.assign({}, {'Content-Type': 'application/json'}, this.auth.generateAuthenticationHeader()); 
           
        this.axios = Axios.create({
            baseURL: apiHost,
            timeout: 30000,
            headers: headers,
            validateStatus: status => true
        });
    }

    get isAuthenticated() {
        return this.auth ? this.auth.isAuthenticated() : false;
    }

    /* Project calls */

    async getProject(projectId: string) : Promise<Project> {

        let result = await this.axios.get(`/projects/${projectId}`);
        if(result.status != 200) {
            const err = new Error("Failed to get project") as EnterpriseServiceError;
            err.statusCode = result.status;
            err.statusMessage = result.statusText;
            throw err;
        }

        return result.data as Project;
    }
    
    async listPublicProjects() : Promise<Array<PublicProject>> {

        let result = await this.axios.get(`/projects?public`);
        if(result.status != 200) {
            const err = new Error("Failed to list projects") as EnterpriseServiceError;
            err.statusCode = result.status;
            err.statusMessage = result.statusText;
            throw err;
        }

        return result.data as Array<PublicProject>;
    }

    async listProjects() : Promise<Array<Project>> {

        if(!this.isAuthenticated) throw new Error("Must be authenticated to call listProjects"); 

        let result = await this.axios.get(`/projects`);
        if(result.status != 200) {
            const err = new Error("Failed to list projects") as EnterpriseServiceError;
            err.statusCode = result.status;
            err.statusMessage = result.statusText;
            throw err;
        }

        return result.data as Array<Project>;
    }

    async updateProject(projectId: string, project: UProject) : Promise<Project> {

        if(!this.isAuthenticated) throw new Error("Must be authenticated to call updateProject"); 

        let result = await this.axios.put(`/projects/${projectId}`, Object.assign({}, project, { id: projectId }));
        if(result.status != 200) {
            const err = new Error("Failed to update project") as EnterpriseServiceError;
            err.statusCode = result.status;
            err.statusMessage = result.statusText;
            throw err;
        }

        return result.data as Project;
    }


    /* Lenses calls */

    // Eventually we'll have complex querying; for now, nothing fancy.
    async listLensRefs(projectId: string) : Promise<Array<LensRef>> {
        if(!this.isAuthenticated) throw new Error("Must be authenticated to call listLensRef"); 

        let result = await this.axios.get(`/projects/${projectId}/lensrefs`);
        if(result.status != 200) {
            const err = new Error("Failed to list lens refs") as EnterpriseServiceError;
            err.statusCode = result.status;
            err.statusMessage = result.statusText;
            throw err;
        }

        return result.data as Array<LensRef>; 
    }
    /*
    async listLensRefs(projectId: string, search?: string, limit?: number, continuationToken?: string) : Promise<Array<LensRefs>> {
        if(!this.isAuthenticated) throw new Error("Must be authenticated to call listLenses"); 

        let query = "";
        if(typeof search !== 'undefined' && search.trim() != "")  {
            query += (query ? "&" : "?") + `search=${search}`;
        }
        if(typeof limit !== 'undefined' && Math.floor(limit) > 0)  {
            query += (query ? "&" : "?") + `limit=${Math.floor(limit)}`;
        }
        if(typeof continuationToken !== 'undefined' && continuationToken != null)  {
            query += (query ? "&" : "?") + `continuation=${continuationToken}`;
        }

        let result = await this.axios.get(`/projects/${projectId}/lensrefs${query}`);
        if(result.status != 200) {
            const err = new Error("Failed to get lens refs") as EnterpriseServiceError;
            err.statusCode = result.status;
            err.statusMessage = result.statusText;
            throw err;
        }

        return result.data as Array<LensRef>; 
    }*/

    async createLensRef(projectId: string, lensRef: LensRef) : Promise<LensRef> {
        if(!this.isAuthenticated) throw new Error("Must be authenticated to call createLensRef"); 

        let result = await this.axios.post(`/projects/${projectId}/lensrefs/${lensRef.id}`, lensRef);
        if(result.status != 200) {
            const err = new Error("Failed to create lens ref") as EnterpriseServiceError;
            err.statusCode = result.status;
            err.statusMessage = result.statusText;
            throw err;
        }
        return result.data as LensRef;
    }

    async deleteLensRef(projectId: string, lensRefId: string) : Promise<void> {
        if(!this.isAuthenticated) throw new Error("Must be authenticated to call deleteLensRef"); 

        let result = await this.axios.delete(`/projects/${projectId}/lensrefs/${lensRefId}`);
        if(result.status != 200) {
            const err = new Error("Failed to delete lens ref") as EnterpriseServiceError;
            err.statusCode = result.status;
            err.statusMessage = result.statusText;
            throw err;
        }
    }

    async updateLensRef(projectId: string, lensRefId: string, lensRef: ULensRef) : Promise<LensRef> {
        if(!this.isAuthenticated) throw new Error("Must be authenticated to call updateLensRef"); 

        let result = await this.axios.put(`/projects/${projectId}/lensrefs/${lensRefId}`, lensRef);
        if(result.status != 200) {
            const err = new Error("Failed to update lens ref") as EnterpriseServiceError;
            err.statusCode = result.status;
            err.statusMessage = result.statusText;
            throw err;
        }
        return result.data as LensRef;
    }

    async createLensRefBatch(batch: LensRefBatchRequest) : Promise<LensRefBatchResponse> {
        if(!this.isAuthenticated) throw new Error("Must be authenticated to call createLensRefBatch"); 

        let result = await this.axios.post(`/lensref-batches`, batch);
        if(result.status != 200) {
            const err = new Error("Failed to create lens ref batch") as EnterpriseServiceError;
            err.statusCode = result.status;
            err.statusMessage = result.statusText;
            throw err;
        }
        return result.data as LensRefBatchResponse;
    }

    async listUsers(projectId: string) : Promise<Array<User>> {
        if(!this.isAuthenticated) throw new Error("Must be authenticated to call listUsers"); 

        let result = await this.axios.get(`/projects/${projectId}/users`);
        if(result.status != 200) {
            const err = new Error("Failed to list users") as EnterpriseServiceError;
            err.statusCode = result.status;
            err.statusMessage = result.statusText;
            throw err;
        }

        return result.data as Array<User>; 
    }
    
    async createUser(projectId: string, user: CUser) : Promise<User> {
        if(!this.isAuthenticated) throw new Error("Must be authenticated to call createUser"); 

        let result = await this.axios.post(`/projects/${projectId}/users`, user);
        if(result.status != 200) {
            const err = new Error("Failed to create user") as EnterpriseServiceError;
            err.statusCode = result.status;
            err.statusMessage = result.statusText;
            throw err;
        }
        return result.data as User;
    }
    
    async updateUser(projectId: string, userId: string, user: UUser) : Promise<User> {
        if(!this.isAuthenticated) throw new Error("Must be authenticated to call updateUser"); 

        let result = await this.axios.put(`/projects/${projectId}/users/${userId}`, user);
        if(result.status != 200) {
            const err = new Error("Failed to update user") as EnterpriseServiceError;
            err.statusCode = result.status;
            err.statusMessage = result.statusText;
            throw err;
        }
        return result.data as User;
    }

    async deleteUser(projectId: string, userId: string) : Promise<void> {
        if(!this.isAuthenticated) throw new Error("Must be authenticated to call deleteUser"); 

        let result = await this.axios.delete(`/projects/${projectId}/users/${userId}`);
        if(result.status != 200) {
            const err = new Error("Failed to delete user") as EnterpriseServiceError;
            err.statusCode = result.status;
            err.statusMessage = result.statusText;
            throw err;
        }
    }

    
    async listInvitations(projectId: string) : Promise<Array<Invitation>> {
        if(!this.isAuthenticated) throw new Error("Must be authenticated to call listInvitations"); 

        let result = await this.axios.get(`/projects/${projectId}/invitations`);
        if(result.status != 200) {
            const err = new Error("Failed to list invitations") as EnterpriseServiceError;
            err.statusCode = result.status;
            err.statusMessage = result.statusText;
            throw err;
        }

        return result.data as Array<Invitation>; 
    }
    
    async createInvitation(projectId: string, invitation: CInvitation) : Promise<Invitation> {
        if(!this.isAuthenticated) throw new Error("Must be authenticated to call createInvitation"); 

        let result = await this.axios.post(`/projects/${projectId}/invitations`, invitation);
        if(result.status != 200) {
            const err = new Error("Failed to create invitation") as EnterpriseServiceError;
            err.statusCode = result.status;
            err.statusMessage = result.statusText;
            throw err;
        }
        return result.data as Invitation;
    }
    
    async deleteInvitation(projectId: string, invitationId: string) : Promise<void> {
        if(!this.isAuthenticated) throw new Error("Must be authenticated to call deleteInvitation"); 

        let result = await this.axios.delete(`/projects/${projectId}/invitations/${invitationId}`);
        if(result.status != 200) {
            const err = new Error("Failed to delete invitation") as EnterpriseServiceError;
            err.statusCode = result.status;
            err.statusMessage = result.statusText;
            throw err;
        }
    }

}
