import decryptWebhook, { DecryptionErrorCode, DecryptionError } from './decryptWebhook';
import { WebhookRequestBody } from '../types';
import validateWebhookSignature  from './validateWebhookSignature';

const SIGNATURE_HEADER_KEY = "X-Lens-Signature";
const VALIDATION_GRACE_MS = 5 * 60 * 1000; // 5 minutes 

function createWebhookMiddleware(privateKey: string) {
    return function webhookMiddleware(req : any, res : any, next : any) : void {
        try {
            // Extract signature header.
            const sSignature = req.get(SIGNATURE_HEADER_KEY);

            if(!sSignature) {
                res.status(400).send(`Header ${SIGNATURE_HEADER_KEY} is required, but missing.`);
            } else {
                
                // Separate the timestamp from the signature.
                const [timestamp, signature] = sSignature.split(':');

                // Both need to be valid.
                if(!timestamp || !signature) {
                    res.status(400).send(`Header ${SIGNATURE_HEADER_KEY} is malformed.`);
                } else {

                    // Make sure body exists.
                    const encryptedBody = req.body;
                    if(!encryptedBody) {
                        res.status(400).send(`Request body is missing. Did you forget to run 'bodyParser' first?: "bodyParser.json({ strict: false })"`);
                    } else if(!encryptedBody.cipherText) {
                        res.status(400).send(`Request body is missing property 'cipherText'.`);
                    } else if(!encryptedBody.iv) {
                        res.status(400).send(`Request body is missing property 'iv'.`);
                    }
                    // First validate the signature.
                    // If the timestamp is off by over 5 minutes, don't allow it.
                    else if(Math.abs(Date.now() - timestamp) > VALIDATION_GRACE_MS) {
                        res.status(400).send(`Failed to validate signature: signature timestamp is out of date`);
                    }
                    // Then validate the signature. Throw if validation fails.
                    else if(!validateWebhookSignature(JSON.stringify(encryptedBody), timestamp, signature, privateKey)) {
                        res.status(400).send(`Failed to validate signature.`);
                    } else {

                        let newBody : WebhookRequestBody;
                        try {
                            newBody = decryptWebhook(encryptedBody, privateKey);
                        } catch(err) {
                            const dErr = err as DecryptionError;
                            if(dErr.code == DecryptionErrorCode.FailedToParseJSON) {
                                res.status(400).send(`Failed to decrypt body: encrypted body was not valid JSON.`)
                            } else if(dErr.code == DecryptionErrorCode.General) {
                                res.status(400).send(`Failed to decrypt body.`);
                            } else {
                                // If we don't know what it is, rethrow it and catch it with the
                                // general handling.
                                throw err;
                            }
                        }

                        if(newBody) {
                            // Perform validation on newBody, to make sure it has all it's
                            // bits and bobs

                            // Now replace the body
                            req.originalBody = req.body;
                            req.body = newBody;

                            // Continue on.
                            next();
                        }
                    }
                }
            }
        }
        catch(err) {
            next(err);
        }
    }
}

export default createWebhookMiddleware;