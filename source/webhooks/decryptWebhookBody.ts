import crypto from 'crypto';
import { EncryptedMetaData, symmetricDecrypt } from '@mylens/lens-crypto';

function decryptWebhookBody(encryptedBody: EncryptedMetaData, privateKey: string) : string {
    return symmetricDecrypt(privateKey, encryptedBody);
}

export default decryptWebhookBody;