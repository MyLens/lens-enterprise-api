import { EncryptedMetaData } from '@mylens/lens-crypto';
import decryptWebhookBody from './decryptWebhookBody';
import { WebhookRequestBody } from '../types';

export interface DecryptionError extends Error {
    code: DecryptionErrorCode 
    innerError?: Error
}

function createError(error: Error | string, code?: DecryptionErrorCode) {
    const isWrappingError = typeof error === 'object';
    const err : DecryptionError = Object.assign(new Error(isWrappingError ? (error as Error).message : error as string),
        { code: code || DecryptionErrorCode.General });
    if(isWrappingError) err.innerError = error as Error;

    return err;
}

export enum DecryptionErrorCode {
    General='general',
    FailedToParseJSON='json'
}


function decryptWebhook(
    encryptedBody: EncryptedMetaData,
    privateKey: string
) : WebhookRequestBody {

    // Decrypt the information
    let decryptedBody: string; 
    try {
        decryptedBody = decryptWebhookBody(encryptedBody, privateKey);
    } catch(err) {
        throw createError(err);
    }

    // And parse and return it
    try {
        return JSON.parse(decryptedBody) as WebhookRequestBody;
    } catch(err) {
        throw createError(err, DecryptionErrorCode.FailedToParseJSON);
    }
}

export default decryptWebhook;
