import crypto from 'crypto';

function validateWebhookSignature(
    body: string,
    timestamp: number,
    signature: string,
    privateKey: string
) : boolean {
    const computedSig = crypto.createHmac("sha256", privateKey)
        .update(body + timestamp)
        .digest("hex");
    return signature == computedSig;
}

export default validateWebhookSignature;