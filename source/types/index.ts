export interface Project {
    id: string,
    name: string,
    description: string,
    categories: Array<Category>, 
    marketingUrl: string,
    isPublic: boolean,
    redirectUrl: string,
    publicKey: string,
    privateKey?: string,
    ownerId: string,
    logoUrl: string, 
	webhooks?: {
        lensCreated?: Array<string>,
        lensDeleted?: Array<string>,
        lensUpdated?: Array<string>,
    },
    request?: any,
    requestMessage?: any,
}

export interface PublicProject {
    id: string,
    name: string,
    description: string,
    categories: Array<Category>, 
    marketingUrl: string,
    isPublic: boolean,
    publicKey: string,
    logoUrl: string,
}

export interface UProject {
    name?: string,
    description?: string,
    categories?: Array<Category>, 
    marketingUrl?: string,
    isPublic?: boolean,
    redirectUrl?: string,
    publicKey?: string,
    privateKey?: string,
    ownerId?: string,
    logoUrl?: string, 
	webhooks?: {
        lensCreated?: Array<string>,
        lensDeleted?: Array<string>,
        lensUpdated?: Array<string>,
    },
    request?: any,
    requestMessage?: any,
}

export enum Category {
    Audio = 'audio',
	Business = 'business',
	Communication = 'communication',
    Design = 'design',
	Education = 'education',
	Enterprise = 'enterprise',
	Entertainment = 'entertainment',
	Finance = 'finance',
	FoodAndDrink = 'foodAndDrink',
	Games = 'games',
	Health = 'health',
	Kids = 'kids',
	Lifestyle = 'lifestyle',
	Local = 'local',
	Magazines = 'magazines',
	Media = 'media',
	Medical = 'medical',
	Movies = 'movies',
	Music = 'music',
	Navigation = 'navigation',
	News = 'news',
	Organization = 'organization',
	Personal = 'personal',
	Photos = 'photos',
	Podcasting = 'podcasting',
	Politics = 'politics',
	Productivity = 'productivity',
	Programming = 'programming',
	Reading = 'reading',
	Reference = 'reference',
    Restaraunt = 'restaraunt',
	Shopping = 'shopping',
	SocialNetworking = 'socialNetworking',
	Sports = 'sports',
	Transportation = 'transportation',
	Travel  =  'travel',
	Utility  =  'utility',
	Videos  =  'videos',
	Weather  =  'weather',
}

export interface LensRef {
    id: string,
    url: string,
    isSelfTarget: boolean,
    target?: LensTarget,
    state?: string,
    ownerId: string,
    created: number,
    isRevoked: boolean
}

export interface ULensRef {
    /* No properties on a lens ref can be updated right now. */
}

export interface LensTarget {
    id: string,
    publicKey?: string,
    privateKey?: string
}

export interface LensRefBatchRequest {
    requests: Array<{
        action: LensRefBatchAction,
        projectId: string,
        id: string
    }>
}

export enum LensRefBatchAction {
    Delete="delete",
    Update="update"
}

export interface LensRefBatchResponse {
    responses: Array<{
        status: number,
        body?: LensRef
    }>
}

export interface Invitation {
    id: string,
    isOwner: boolean,
    name: string,
    created: number
}

export interface CInvitation {
    isOwner: boolean,
    name: string,
}

export enum UserType {
    General="general",
    Owner="owner",
    Credential="credential"
}

export interface User {
    id: string,
    projectId: string,
    type: UserType,
    secretKey?: string,
    name: string,
    created: number
}

export interface UUser {
    type?: UserType,
    name?: string
}

export interface CUser {
    id?: string,
    invitationId?: string,
    type?: UserType,
    name?: string,
}

export interface EnterpriseServiceError extends Error {
    statusCode: number,
    statusMessage: string
}

export interface WebhookRequestBody {
    action: WebhookAction,
    lensRef: LensRef,
    timestamp: number,
}

export enum WebhookAction {
    Create="create",
    Update="update",
    Delete="delete"
}