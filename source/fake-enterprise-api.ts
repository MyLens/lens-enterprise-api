// import {Project, LensRef, EnterpriseServiceError} from './types/'


// export default class FakeEnterpriseApi {
//     constructor(apiHost : string, bsUserData? : any) {
//     }

//     private projects = [
//         {
//             id: "el12345",
//             name: "EarthLock",
//             redirectUrl: "https://trusting-kalam-9eae78.netlify.com/*",
//             publicKey: "PUB1234567890",
//             privateKey: "PRIV0987654321"
//         }
//     ];

//     private lensRefs = [
//         {
//           id: "27221421-9d97-451c-ac63-438a9ce17b4e",
//           url: "https://gaia.blockstack.org/hub/1EfDzuNREbgCjXP6ixi4VPoqDWTZXFHWQv/lenses/27221421-9d97-451c-ac63-438a9ce17b4e.json",
//           isSelfTarget: false,
//           target: {
//               id: "37115939-57aa-4255-a747-9ad6e55932a9",
//               publicKey: "03aac3d9e590256a88bafa298b8297c2f56acafc8284bc06b8b597edee0010ac00",
//               privateKey: "3e39078b62c183d633c2f4dc2b75226dbc2e424bcd98541f38e7daf1a163c8e8",
//           },
//           state: "x321212",
//           created: 1567542848983,
//         },       
//         {
//             id: "17221421-9d97-451c-ac63-438a9ce17b4e",
//             url: "https://gaia.blockstack.org/hub/1EfDzuNREbgCjXP6ixi4VPoqDWTZXFHWQv/lenses/17221421-9d97-451c-ac63-438a9ce17b4e.json",
//             isSelfTarget: false,
//             state: "x521212",
//             created: 1567542848984,
//         },
//         {
//             id: "a62c8145-be5d-4e69-8ec3-a7bd3dfa9a80",
//             url: "https://gaia.blockstack.org/hub/19VL8f9hZcMaecxQDer763AbN8vF1k8XvE/lenses/a62c8145-be5d-4e69-8ec3-a7bd3dfa9a80.json",
//             isSelfTarget: false,
//             target: {
//                 id: "4f1dab28-62a7-485a-847b-0f05eb892253",
//                 publicKey: "039c087d6d075654f757803760944d109e96b790665005f5c9b5501e016a9f30dd",
//                 privateKey: "82f444fa5aae07e0dc085da4a8f3075e2bdd80247e02b4fa7fda1e5253c5d729",
//             },
//             state: "x121212",
//             created: 1567542848983,
//         },
//         {
//             id: "e177fdcc-9f71-4646-ae3f-b47bed5c746e",
//             url: "https://gaia.blockstack.org/hub/19VL8f9hZcMaecxQDer763AbN8vF1k8XvE/lenses/e177fdcc-9f71-4646-ae3f-b47bed5c746e.json",
//             isSelfTarget: false,
//             target: {
//                 id: "aea43a1d-1726-4dda-a5b3-66c0f424e38b",
//                 publicKey: "03a54237f7819c75a683e8a8f34bb8b211afa192f6049f073056639fedf243e7f7",
//                 privateKey: "60ece4b2622234cf0330d871d936a6bd14175bad174ec77e12f317bf1b103c",
//             },
//             state: "x221212",
//             created: 1567542881912,
//         },
//         {
//             id: "90b24e21-aef2-4b23-9c3c-c8c023b11b8b",
//             url: "https://gaia.blockstack.org/hub/19VL8f9hZcMaecxQDer763AbN8vF1k8XvE/lenses/90b24e21-aef2-4b23-9c3c-c8c023b11b8b.json",
//             isSelfTarget: false,
//             target: {
//                 id: "11b5657d-aca3-4657-8e5e-2c9475b67292",
//                 publicKey: "039fb13144a3a57dcd44242708d604041c6e23a9e145363f240cc7fcfaec66ba9f",
//                 privateKey: "84c13e52533db63c77c6aba614640b96f8cc2888eaba61d1ee9ad0eff004eb51",
//             },
//             state: "x321212",
//             created: 1567542887528,
//         },
//         {
//             id: "52617fa9-d2d1-4962-97e0-4a634b891242",
//             url: "https://gaia.blockstack.org/hub/18AWDJT1SDY2ndQYY2Wkut6a18FPsVjQjU/lenses/52617fa9-d2d1-4962-97e0-4a634b891242.json",
//             isSelfTarget: false,
//             target: {
//                 id: "f0c477c2-7e33-4cc5-9f21-df147f8a9405",
//                 publicKey: "03ced3d614d4b2139070c6ba57cc88c2487d901fc2c10d356e03e31e28c3072acf",
//                 privateKey: "30d4372000c001e796a438765fee4d191df9b9e2b21eb0936317a95be0a2f572",
//             },
//             state: "x321212",
//             created: 1567542887533,
//         }
//     ];

//     private async delay(time: number) : Promise<any> {
//         return new Promise((r) => {
//             setTimeout(() => {
//                 r();
//             }, time);
//         })
//     }

//     /* Project calls */

//     async getProject(projectId: string) : Promise<Project> {

//         await this.delay(250);
//         const project = this.projects.find(p => p.id == projectId);

//         if(!project) {
//             const err = new Error("Failed to get project") as EnterpriseServiceError;
//             err.statusCode = 404;
//             err.statusMessage = "NOT FOUND";
//             throw err;
//         }

//         return Object.assign({}, project) as Project;
//     }

//     async listProjects() : Promise<Array<Project>> {
//         await this.delay(250);

//         return this.projects.map(p => Object.assign({}, p));
//     }

//     async updateProject(projectId: string, project: Project) : Promise<Project> {
//         await this.delay(250);

//         const eProject = this.projects.find(p => p.id == projectId);

//         if(!eProject) {
//             const err = new Error("Failed to get project") as EnterpriseServiceError;
//             err.statusCode = 404;
//             err.statusMessage = "NOT FOUND";
//             throw err;
//         }

//         Object.assign(eProject, {
//             redirectUrl: project.redirectUrl,
//             name: project.name
//         });

//         return Object.assign({}, eProject) as Project;
//     }


//     /* LensRef calls */
//     async listLensRefs(projectId: string) : Promise<Array<LensRef>> {
//         await this.delay(250);

//         return this.lensRefs as Array<LensRef>; 
//     }

// }